<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S01: PHP Basics and Selection Control Structures</title>
</head>
<body>
	<h1>Echoing Values</h1>

	<p><?php echo "Good day $name! Your given email is $email. Your age is $age."; ?></p>
	<p><?php echo "The value of Pi is " . PI . "."; ?></p>

	<p><?php echo $hasTravelledAbroad; ?></p>
	<p><?php echo $hasSymptoms; ?></p>
	<p><?php echo gettype($hasTravelledAbroad); ?></p>
	<p><?php echo var_dump($hasSymptoms); ?></p>

	<p><?php echo $spouse; ?></p>
	<p><?php echo gettype($spouse); ?></p>
	<p><?php echo var_dump($spouse); ?></p>

	<p><?php echo $grades[3]; ?></p>
	<p><?php echo $grades[1]; ?></p>

	<p><?php echo $gradesObj->firstGrading; ?></p>
	<p><?php echo $personObj->address->state; ?></p>

	<h1>Operators</h1>
	<p>x: <?php echo $x;?></p>
	<p>y: <?php echo $y;?></p>

	<h2>Arithmetic Operators</h2>
	<p>Sum: <?php echo $x + $y; ?></p>
	<p>Difference: <?php echo $x - $y; ?></p>
	<p>Product: <?php echo $x * $y; ?></p>
	<p>Quotient: <?php echo $x / $y; ?></p>

	<h2>Equality Operators</h2>
	<p>Loose Equality: <?php echo var_dump($x == '1342.14'); ?></p>
	<p>Strict Equality: <?php echo var_dump($x === '1342.14'); ?></p>
	<p>Loose Equality: <?php echo var_dump($x != '1342.14'); ?></p>
	<p>Strict Equality: <?php echo var_dump($x !== '1342.14'); ?></p>

	<h2>Greater/Lesser Operators</h2>
	<p>Is Lesser: <?php echo var_dump($x < $y); ?></p>
	<p>Is Greater: <?php echo var_dump($x > $y); ?></p>

	<h2>Logical Operators</h2>
	<p>Are All Requirements Met: <?php echo var_dump($isLegalAge && $isRegistered) ?></p>
	<p>Are Some Requirements Met: <?php echo var_dump($isLegalAge || $isRegistered) ?></p>
	<p>Are Some Requirements Not Met: <?php echo var_dump($isLegalAge && !$isRegistered) ?></p>

	<h1>Function</h1>
	<p>Full Name: <?php echo getFullName('John', 'D', 'Smith'); ?></p>

	<h1>Selection Control Structures</h1>
	<h2>If-Elseif-Else Statement</h2>
	<p><?php echo determineTyphoonIntensity(12) ?></p>

	<h2>Switch Statement</h2>
	<p><?php echo determineComputerUser(6) ?></p>

	<h2>Ternary Operator</h2>
	<p><?php echo var_dump(isUnderAge(78)) ?></p>

	<h2>Try-Catch-Finally</h2>
	<p><?php echo greeting(12) ?></p>
</body>
</html>